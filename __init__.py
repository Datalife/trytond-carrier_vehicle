# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .carrier import Carrier, Vehicle, VehicleType


def register():
    Pool.register(
        Carrier,
        VehicleType,
        Vehicle,
        module='carrier_vehicle', type_='model')
