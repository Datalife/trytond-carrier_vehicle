# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Carrier', 'Vehicle', 'VehicleType']


class Carrier(metaclass=PoolMeta):
    __name__ = 'carrier'

    vehicles = fields.One2Many('carrier.vehicle', 'carrier', 'Vehicles')


# TODO: identify if vehicle is for input or output
class Vehicle(DeactivableMixin, ModelView, ModelSQL):
    """Carrier vehicle"""
    __name__ = 'carrier.vehicle'
    _rec_name = 'number'

    carrier = fields.Many2One('carrier', "Carrier",
        required=True, select=True, ondelete='CASCADE',
        states={'readonly': ~Eval('active')},
        depends=['active'])
    number = fields.Char("Number", required=True,
        states={'readonly': ~Eval('active')},
        depends=['active'])
    trailer_number = fields.Char("Trailer reg. number",
        states={'readonly': ~Eval('active')},
        depends=['active'])
    driver = fields.Char("Driver",
        states={'readonly': ~Eval('active')},
        depends=['active'])
    driver_identifier = fields.Char("Driver Identifier",
        states={'readonly': ~Eval('active')},
        depends=['active'])
    type = fields.Many2One('carrier.vehicle.type', 'Type',
        select=True, ondelete='RESTRICT', required=True,
        states={'readonly': ~Eval('active')},
        depends=['active'])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        if table_h.column_exist('registration_number'):
            table_h.column_rename('registration_number', 'number')

        if table_h.column_exist('registration_number2'):
            table_h.column_rename('registration_number2', 'trailer_number')

        super().__register__(module_name)

    def get_rec_name(self, name):
        if self.trailer_number:
            return '%s / %s' % (self.number,
              self.trailer_number)
        return self.number


class VehicleType(DeactivableMixin, ModelView, ModelSQL):
    """Carrier vehicle type"""
    __name__ = 'carrier.vehicle.type'

    name = fields.Char('Name', required=True, translate=True,
                       states={'readonly': ~Eval('active')},
                       depends=['active'])
